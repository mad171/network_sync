# -*- coding: utf-8 -*-

##
# @author Ludovic Descateaux, CentraleSupelec, France
# @date 2017-01-17, 2017-02-08
# @version 2.0 [Python 2.7]
#

# -- Standard modules
import sys
import time

# -- Third-party modules
import numpy

# -- MRG modules
sys.path.append("mod")
from mesh import Mesh
from graph import Graph


# -- Constants

# Error
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

# Program description
PROG_NAME = "[GraphTestBuildVertDegree]"
MIN_ARGC = 1
HELP = """
  BRIEF: Tests the list of vertices degrees built with class Graph.
  ARGS:
        [-h] # Displays this description.
"""

# -- main ----------------------------------------------------------------------
def main ( argv=[PROG_NAME] ) :

  # ----------------------------------------------------------------------------
  # -- INITIALIZATION
  # ----------------------------------------------------------------------------

  # -- Graph I/O handler
  dom_io = Mesh()

  # -- Test
  
  # Expected output values
  ref_vert_degree = numpy.array([1,2,4,4,2,5,2,4,2,4,3,1])
  
  # Test result
  test_success = True

  # ----------------------------------------------------------------------------
  # -- ARGUMENTS
  # ----------------------------------------------------------------------------

  # -- check minimum number of arguments
  argc = len(argv)
  if (argc < MIN_ARGC) :
    print HELP
    return EXIT_FAILURE


  # -- set input graph file name (constant)
  ig_file_name = "../data/in/MRG/AP3D-H0750B-SS0-LGM12.vtk"


  # -- print help (-h)
  if ("-h" in argv[MIN_ARGC:]) :
    print HELP
    return EXIT_SUCCESS

  # ----------------------------------------------------------------------------
  # -- INPUT
  # ----------------------------------------------------------------------------
#
  # -- read graph
  print PROG_NAME, "--- Reading graph from", ig_file_name
  dom_io.ReadFromFileVtk(ig_file_name)
  if (dom_io.err_code == Mesh.FAILURE) :
    print PROG_NAME, dom_io.err_msg
    return EXIT_FAILURE


  # -- build graph handler
  dom = Graph(dom_io.numb_node,
              dom_io.numb_elem,
              dom_io.elem2node,
              dom_io.p_elem2node)

  # ----------------------------------------------------------------------------
  # -- PROCESS
  # ----------------------------------------------------------------------------

#
  # -- begin time measurement

  # cpu time
  cpu_btime = time.clock()
  # wall-clock time
  wclock_btime = time.time()


  # ---------------------- test 1 : building from vert2vert

  print PROG_NAME, "--- Testing building from vert2vert"
  dom.BuildVert2Edge()
  dom.BuildVert2Vert()
  dom.BuildVertDegree()

  
  if (not numpy.array_equal(dom.vert_degree, ref_vert_degree)) :
      print('Error building from vert2vert')
      test_success = False

  
  # re-initializing
  dom.vert_degree = numpy.array([])
  dom.vert2vert = numpy.array([])
  dom.p_vert2vert = numpy.array([0])
  
  
  
  # ---------------------- test 2 : building from vert2edge

  print PROG_NAME, "--- Testing building from vert2edge"
  dom.BuildVert2Edge()
  dom.BuildVertDegree()
  
  if (not numpy.array_equal(dom.vert_degree, ref_vert_degree)) :
      print('Error building from vert2edge')
      test_success = False

  #re-initializing
  dom.vert_degree = numpy.array([])
  dom.vert2edge = numpy.array([])
  dom.p_vert2edge = numpy.array([0])



  # ---------------------- test 3 : building from edge2vert

  print PROG_NAME, "--- Testing building from edge2vert"
  dom.BuildVertDegree()

  if (not numpy.array_equal(dom.vert_degree, ref_vert_degree)) :
      print('Error building from edge2vert')
      test_success = False



  # -- end time measurement

  # cpu time
  cpu_etime = time.clock()
  # wall-clock time
  wclock_etime = time.time()


  # ----------------------------------------------------------------------------
  # -- OUTPUT
  # ----------------------------------------------------------------------------

  # -- print result
  if (test_success) :
    print PROG_NAME, "*** Result: SUCCESS"
  else :
    print PROG_NAME, "*** Result: FAILURE"
    print dom.err_msg

  # -- print time measurement
  print PROG_NAME, "*** CPU: {:.3f} sec.".format(cpu_etime - cpu_btime)
  print PROG_NAME,\
        "*** Wall-clock: {:.3f} sec.".format(wclock_etime - wclock_btime)
  #{:.3f} format d'affichage, f pour flotant, 3 pour 3 chiffres après la virgule
  
  return EXIT_SUCCESS

# END def main ( argc, argv ) :
# ------------------------------------------------------------------------------

if __name__ == "__main__" :
  main(sys.argv)