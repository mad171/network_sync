# -*- coding: utf-8 -*-

##
# @author Ludovic Descateaux, IS1260-PC01, CentraleSupelec, France
# @date 2017-01-17, 2017-02-08
# @version 2.0 [Python 2.7]
#

# -- Standard modules
import sys
import numpy
import math

# -- MRG modules
sys.path.append("mod")
from graph import Graph
from netsim import NetSim 
from mesh import Mesh
from meshviz import MeshViz


# Program description
PROG_NAME = "[simulation]"
Mesh_file_name = "../data/in/MRG/AP3D-H0750B-S0-LGMb120.vtk"
coloration_to_apply = 1   
HELP = """
  BRIEF: A program which aims at running a simulation of synchronisation of an entire network
  ARGS: 
        [Mesh_file_name] :  adress of the file which contains the mesh you want 
                            to apply the simulation to      
        [coloration_to_apply] :  int number  which represents the type of coloration you
                        want to apply,0-NoColoration, 1-WelshPowell, 2-DSATUR
        [-h] # Displays this description.
"""

# -- main ----------------------------------------------------------------------
def main (PROG_NAME, Mesh_file_name, coloration_to_apply) :

  # ----------------------------------------------------------------------------
  # -- INITIALIZATION
  # ----------------------------------------------------------------------------

  # -- Graph handler
  
  Mesh_to_be_tested = Mesh()
  Graph_to_be_tested = Graph()
  Adjoint = Graph()
  MeshViz1 = MeshViz()

  # -- Output values   
  
  Units_of_Time_to_synchronise = 0

  # ----------------------------------------------------------------------------
  # -- INPUT
  # ----------------------------------------------------------------------------

  # -- read mesh

  print PROG_NAME, "--- Reading graph from", Mesh_file_name
  Mesh_to_be_tested.ReadFromFileVtk(Mesh_file_name)
    
  Graph_to_be_tested = Graph(Mesh_to_be_tested.numb_node,
              Mesh_to_be_tested.numb_elem,
              Mesh_to_be_tested.elem2node,
              Mesh_to_be_tested.p_elem2node)  
        
  nodes_loads_2D = Mesh_to_be_tested.ReadFieldFromFileVtk(Mesh_file_name, "subdomain_numb_nodes")
  nodes_loads = numpy.array([0]*len(nodes_loads_2D))
  for k in range (len(nodes_loads_2D)) :
      nodes_loads[k] = nodes_loads_2D[k][0]
  
  edges_loads_2D = Mesh_to_be_tested.ReadFieldFromFileVtk(Mesh_file_name, "subdomain2numb_interface_node")
  edges_loads = numpy.array([0]*len(edges_loads_2D))
  for k in range (len(edges_loads_2D)):
      edges_loads[k] = edges_loads_2D[k][0]



  # ----------------------------------------------------------------------------
  # -- PROCESS
  # ----------------------------------------------------------------------------
      
  # -- If Coloration with Welsh Powell algorithm 

  if coloration_to_apply == 1 or coloration_to_apply == 2 :
      
      print "\n", PROG_NAME, '---Building Edge2Edge from ', Mesh_file_name
      Graph_to_be_tested.BuildEdge2Edge()
      print "\n", PROG_NAME, "--- Link : G.Edge2Edge() = Adjoint_de_G.vert2vert from ",  Mesh_file_name
      Adjoint.vert2vert = Graph_to_be_tested.edge2edge
      Adjoint.p_vert2vert = Graph_to_be_tested.p_edge2edge
      Adjoint.numb_vert = Graph_to_be_tested.numb_edge      
      print "\n", PROG_NAME, "--- Building Vert_Degree from Adjoint.Vert2Vert from ",  Mesh_file_name      
      Adjoint.BuildVertDegree()
      print "\n", PROG_NAME, "--- Building Vert_Degree from G.Edge2Vert from ",  Mesh_file_name      
      Graph_to_be_tested.BuildVertDegree()  
      

      if coloration_to_apply == 1:
          # -- Apply Welsh-Powell algorithm
          print '---Coloring the graph_adjoint with Welsh-Powell algorithm'
          Adjoint.ColorVertByWelshPowell()  
          WP_efficiency = (max(Graph_to_be_tested.vert_degree)/(Adjoint.numb_color))
          Graph_to_be_tested.edge_color = Adjoint.vert_color
          print "\n", PROG_NAME, "--- Building Vert2Edge from G.edge2vert from ",  Mesh_file_name
          Graph_to_be_tested.BuildVert2Edge()
          print "\n", PROG_NAME, "--- Sorting Vert2Edge with the Welsh-Powell coloration"
          Graph_to_be_tested.SortVert2EdgeByColor()
          print "\n", PROG_NAME, "--- Building a final sorted Vert2Vert for ",  Mesh_file_name
          Graph_to_be_tested.BuildVert2Vert()
      
      else :
      # -- Apply DSATUR algorithm
          print '---Coloring the graph_adjoint with DSATUR algorithm'
          Adjoint.ColorVertByDSATUR()    
          DSat_efficiency = (max(Graph_to_be_tested.vert_degree))/(Adjoint.numb_color)
          Graph_to_be_tested.edge_color = Adjoint.vert_color
          print "\n", PROG_NAME, "--- Building Vert2Edge from G.edge2vert from ",  Mesh_file_name
          Graph_to_be_tested.BuildVert2Edge()
          print "\n", PROG_NAME, "--- Sorting Vert2Edge with the DSATUR coloration"
          Graph_to_be_tested.SortVert2EdgeByColor()
          print "\n", PROG_NAME, "--- Building a final sorted Vert2Vert for ",  Mesh_file_name
          Graph_to_be_tested.BuildVert2Vert()
  

  # -- If No Coloration 

  else :
      print "\n", PROG_NAME, "--- Building Vert_Degree from G.Edge2Vert from ",  Mesh_file_name      
      Graph_to_be_tested.BuildVertDegree()  
      print PROG_NAME, "---Building Vert2Edge"
      Graph_to_be_tested.BuildVert2Edge()
      print PROG_NAME, "---Building Vert2Vert"
      Graph_to_be_tested.BuildVert2Vert()


  # -- setting the simulation class corresponding to the graph
 
  GraphSim = NetSim(Graph_to_be_tested.numb_vert,
                    Graph_to_be_tested.numb_edge, 
                    Graph_to_be_tested.vert2edge,
                    Graph_to_be_tested.vert2vert,
                    Graph_to_be_tested.p_vert2vert,
                    nodes_loads,
                    edges_loads,
                    1)
  
  
  
  # -- setting the visualisation

  MeshViz1.ReadMeshFromFile(Mesh_file_name)
  
  MeshViz1.SelectPointData("subdomain_numb_nodes")  
  MeshViz1.SelectCellData("subdomain2numb_interface_node")

  nodes_max_load = max(nodes_loads)
  edges_max_load = max(edges_loads)
  
  MeshViz1.BuildPointColorScale (0,nodes_max_load,scale_type = "rainbow" )
  MeshViz1.BuildCellColorScale (0,edges_max_load,scale_type = "rainbow" )
  MeshViz1.BuildGlyph (sphere_radius = 0.5)

  MeshViz1.Config (bg_color = (0,0,0),
                   win_size = (1000, 800),
                   win_pos = (10, 30),
                   cam_azimuth = 0,
                   cam_elevation = 0,
                   cam_zoom = 1.2 )
  
  
  # -- Running the simulation and visualisation

  compteur = 0 
  while not(GraphSim.flag_end):
      compteur +=1
      GraphSim.Step()
     
      if (compteur % 100) == 0 :
          MeshViz1.Config (bg_color = (0,0,0),
                   win_size = (1000, 800),
                   win_pos = (10, 30),
                   cam_azimuth = 0,
                   cam_elevation = 0,
                   cam_zoom = 1.2 )
          
          if coloration_to_apply == 1 :
              colo = "Welsh-Powell"
          elif coloration_to_apply == 2 :
              colo = "DSATUR"
          else :
              colo = "Sans Coloration"
          save_adress = "../data/out/Visualisation/Vrac/" + colo + "/" + str(compteur) + ".jpg"
          
          MeshViz1.Render (GraphSim.node_step, GraphSim.link_step)
          MeshViz1.WriteScreenshotToFile(save_adress, file_format = "jpg")
      
  MeshViz1.Close()
  Units_of_Time_to_synchronise = GraphSim.numb_step[0]

     
  # ----------------------------------------------------------------------------
  # -- OUTPUT
  # ----------------------------------------------------------------------------
  
  # -- reporting errors
  print Mesh_to_be_tested.err_msg
  print Graph_to_be_tested.err_msg
  print Adjoint.err_msg
  print GraphSim.err_msg


  # -- saving the results in a text_file    
  print '\n\n', PROG_NAME, '---WRITING RESULTS'
    
  # -- open file   
  result_file_name = "../data/out/simulation_results.txt" 
  try :
      p_file = open(result_file_name, "a")
  except :
      print " Error: cannot open " + result_file_name
      return


  # -- write in

  # write the name of the graphs tested
  p_file.write("\n\n\nNEW SIMULATION for the Graph " + Mesh_file_name)
  p_file.write("\n\tnodes : " + str(Graph_to_be_tested.numb_vert))
  p_file.write("\n\tlinks : " + str(Graph_to_be_tested.numb_edge))
  p_file.write("\n\tdegree max : " + str(max(Graph_to_be_tested.vert_degree)))
      
      
  # structure statistics
  L1 = numpy.array([int(x) for x in Graph_to_be_tested.vert_degree])
  l = len(L1)
  Average_X = float(sum(L1))/l
  L2 = numpy.array([long(x)**2 for x in Graph_to_be_tested.vert_degree])
  Average_X2 = float(sum(L2))/l
      
  average_degree = "%.2f" % (Average_X)
  standard_deviation_among_degrees = "%.2f" % (math.sqrt((Average_X2) - (Average_X)**2))
  p_file.write("\n\taverage degree : " + str(average_degree))
  p_file.write("\n\tstandard deviation among degrees : " + str(standard_deviation_among_degrees))
      
      
  # loads statistics
  #NODES
  L1 = numpy.array([int(x) for x in nodes_loads])
  l = len(L1)
  Average_X = float(sum(L1))/l
  L2 = numpy.array([long(x)**2 for x in nodes_loads])
  Average_X2 = float(sum(L2))/l
      
  p_file.write("\n\n ---NODES LOADS---")
  average_nodes_load = "%.2f" % (Average_X)
  standard_deviation_among_nodes_loads = "%.2f" % (math.sqrt((Average_X2) - (Average_X)**2))
  p_file.write("\n\taverage node load : " + str(average_nodes_load))
  p_file.write("\n\tstandard deviation among nodes loads : " + str(standard_deviation_among_nodes_loads))
        
  #EDGES
  L1 = numpy.array([int(x) for x in edges_loads])
  l = len(L1)
  Average_X = float(sum(L1))/l
  L2 = numpy.array([long(x)**2 for x in edges_loads])
  Average_X2 = float(sum(L2))/l
      
  p_file.write("\n\n ---EDGES LOADS---")
  average_edges_load = "%.2f" % (Average_X)
  standard_deviation_among_edges_loads = math.sqrt((Average_X2) - (Average_X)**2)
  p_file.write("\n\taverage edge load : " + str(average_edges_load))
  p_file.write("\n\tstandard deviation among edges loads : " + str(standard_deviation_among_edges_loads))
  
  p_file.write("\n\n ---RESULTS---")


  # -- write the coloration mode
  if coloration_to_apply == 1 :
      p_file.write("\n\n\tColoration : Welsh-Powell")
      p_file.write('\n\tWP-efficiency' + "\t" + str(WP_efficiency))

  elif coloration_to_apply == 2 :
      p_file.write("\n\n\tColoration : DSATUR")
      p_file.write('\n\tDSat-efficiency' + "\t" + str(DSat_efficiency))

  else :
      p_file.write("\n\n\tColoration : No coloration")
     
  # write the number of step 
  p_file.write('\n\tUnits_of_Time_to_synchronise ' + str(Units_of_Time_to_synchronise))
    
    
  # -- open new file   
  result_file_name = "../data/out/simulation_ordre_des_connexions.txt" 
  try :
      p_file = open(result_file_name, "w")
  except :
      print " Error: cannot open " + result_file_name
      return
      
  # write the name of the graphs tested
  p_file.write("RECOMMANDED CONNEXIONS ORDER for the Graph " + Mesh_file_name)    
  p_file.write("\nColoration : ")
  if coloration_to_apply == 1 :
      p_file.write("Welsh-Powell\n\n")
  elif coloration_to_apply == 2 :
      p_file.write("DSATUR\n\n")
  else :
      p_file.write("No Coloration\n\n")
      
  for i in range (len(Graph_to_be_tested.p_vert2vert)-1):
      p_file.write("\nserveur" + str(i) + str(Graph_to_be_tested.vert2vert[Graph_to_be_tested.p_vert2vert[i] : Graph_to_be_tested.p_vert2vert[i+1]]))



  return 

# END def main ( argc, argv ) :
# ------------------------------------------------------------------------------


main(PROG_NAME, Mesh_file_name, coloration_to_apply)
