# -*- coding: utf-8 -*-

##
# @author Ludovic Descateaux, CentraleSupelec, France
# @date 2017-01-17, 2017-02-12
# @version 2.0 [Python 2.7]
#

# -- Standard modules
import sys
import time

# -- Third-party modules
import numpy

# -- MRG modules
sys.path.append("mod")
from graph import Graph


# -- Constants

# Error
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

# Program description
PROG_NAME = "[GraphTestSortVert2EdgeByColor]"
MIN_ARGC = 1
HELP = """
  BRIEF: Tests the Sorting of Vert2Edge using the coloration.
  ARGS:
        [-h] # Displays this description.
"""

# -- main ----------------------------------------------------------------------
def main ( argv=[PROG_NAME] ) :

  # ----------------------------------------------------------------------------
  # -- INITIALIZATION
  # ----------------------------------------------------------------------------

  # -- Test
  
  # Expected output values
  ref_Vert2EdgeSort = numpy.array([0,1,2,0,3,2,4,1,5,4,5,6,6,3])

  # Test result
  test_success = True


  # ----------------------------------------------------------------------------
  # -- ARGUMENTS
  # ----------------------------------------------------------------------------

  # -- check minimum number of arguments
  argc = len(argv)
  if (argc < MIN_ARGC) :
    print HELP
    return EXIT_FAILURE


  # -- print help (-h)
  if ("-h" in argv[MIN_ARGC:]) :
    print HELP
    return EXIT_SUCCESS


  # ----------------------------------------------------------------------------
  # -- INPUT
  # ----------------------------------------------------------------------------

  # -- Start with hand writen vert2edge, p_vert2edge and edge_color
  print PROG_NAME, "--- Starting with hand-writen vert2edge, p_vert2edge and edge_color"

  dom = Graph()
  dom.vert2edge = [0,1,0,2,3,1,2,4,4,5,5,6,3,6]
  dom.p_vert2edge = [0,2,5,8,10,12,14]
  dom.edge_color = [1,2,0,2,1,0,1]


  # ----------------------------------------------------------------------------
  # -- PROCESS
  # ----------------------------------------------------------------------------

  # -- begin time measurement

  # cpu time
  cpu_btime = time.clock()
  # wall-clock time
  wclock_btime = time.time()


  # -- Test 
  print PROG_NAME, "--- Testing"
  dom.SortVert2EdgeByColor()
  if (not numpy.array_equal(dom.vert2edge, ref_Vert2EdgeSort)) :
      print('Error in sorting edge2vert')
      test_success = False


  # -- end time measurement

  # cpu time
  cpu_etime = time.clock()
  # wall-clock time
  wclock_etime = time.time()


  # ----------------------------------------------------------------------------
  # -- OUTPUT
  # ----------------------------------------------------------------------------

  # -- print result
  if (test_success) :
    print PROG_NAME, "*** Result: SUCCESS"
  else :
    print PROG_NAME, "*** Result: FAILURE"
    print dom.err_msg


  # -- print time measurement
  print PROG_NAME, "*** CPU: {:.3f} sec.".format(cpu_etime - cpu_btime)
  print PROG_NAME,\
        "*** Wall-clock: {:.3f} sec.".format(wclock_etime - wclock_btime)
  #{:.3f} format d'affichage, f pour flotant, 3 pour 3 chiffres après la virgule
  
  return EXIT_SUCCESS

# END def main ( argc, argv ) :
# ------------------------------------------------------------------------------

if __name__ == "__main__" :
  main(sys.argv)