# -*- coding: utf-8 -*-

##
# @author Ludovic Descateaux, IS1260-PC01, CentraleSupelec, France
# @date 2017-01-17, 2017-02-12
# @version 2.0 [Python 2.7]
#

# -- Standard modules
import sys
import time
import numpy
import math

# -- MRG modules
sys.path.append("mod")
from graph import Graph
from mesh import Mesh

# -- Constants

# Error
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

# Program description

PROG_NAME = "[nco_benchmark]"

# @ BRIEF: A Benchmark program which aims at compairing Welsh-Powell and DSATUR 
#   coloring algorithms on several graphs.
# @ ARGS: 
#        [Graph_file_name] : adress of the file which contains the description 
#                            of the graph you want to test
#        [-h] # Displays this description.
# @ USE : put the adress of the graph you want to test in 
#         Adresses_Of_The_Graphs_To_Test at the end of the code


# -- main ----------------------------------------------------------------------
def main (PROG_NAME, Graph_file_name) :

  # ----------------------------------------------------------------------------
  # -- INITIALIZATION
  # ----------------------------------------------------------------------------

  # -- Graph handler

  Graph_to_be_tested = Graph()
  Adjoint = Graph()
  Mesh_to_be_tested = Mesh()
  graph_type = Graph_file_name[len(Graph_file_name)-3 :]
  print graph_type
  
  # -- Output values
  Execution_time_WP = 0
  Execution_time_DSat = 0
  WP_efficiency = 0
  DSat_efficiency = 0


  # ----------------------------------------------------------------------------
  # -- INPUT
  # ----------------------------------------------------------------------------


  # -- read graphs

  if graph_type == 'col' :
      print PROG_NAME, "--- Reading graph from Dimacs ", Graph_file_name
      Graph_to_be_tested.ReadFromFileDimacs(Graph_file_name)     
      if (Graph_to_be_tested.err_code == Graph.FAILURE) :
          print PROG_NAME, Graph_to_be_tested.err_msg
          return EXIT_FAILURE 
          
  else :
      print PROG_NAME, "--- Reading graph from vtk ", Graph_file_name
      Mesh_to_be_tested.ReadFromFileVtk(Graph_file_name)
      if (Mesh_to_be_tested.err_code == Graph.FAILURE) :
          print PROG_NAME, Mesh_to_be_tested.err_msg
          return EXIT_FAILURE   
      else :
          Graph_to_be_tested = Graph(Mesh_to_be_tested.numb_node,
                                     Mesh_to_be_tested.numb_elem,
                                     Mesh_to_be_tested.elem2node,
                                     Mesh_to_be_tested.p_elem2node)       
      
    

  # ----------------------------------------------------------------------------
  # -- PROCESS
  # ----------------------------------------------------------------------------

  # -- build vert2vert, p_vert2vert, and vert_degree for the graph
  
  print "\n", PROG_NAME, '---Building Edge2Edge from ', Graph_file_name
  Graph_to_be_tested.BuildEdge2Edge()
  print "\n", PROG_NAME, "--- Link : G.Edge2Edge() = Adjoint_de_G.vert2vert from ",  Graph_file_name
  Adjoint.vert2vert = Graph_to_be_tested.edge2edge
  Adjoint.p_vert2vert = Graph_to_be_tested.p_edge2edge
  Adjoint.numb_vert = Graph_to_be_tested.numb_edge      
  print "\n", PROG_NAME, "--- Building Vert_Degree from Adjoint.Vert2Vert from ",  Graph_file_name      
  Adjoint.BuildVertDegree()
  print "\n", PROG_NAME, "--- Building Vert_Degree from G.edge2vert from ",  Graph_file_name
  Graph_to_be_tested.BuildVertDegree()



  # -- Apply Welsh-Powell algorithm 

  # -- begin time measurement

  # wall-clock time
  wclock_btime = time.time()      
         
  print '---Coloring the graph_adjoint with Welsh-Powell algorithm'
  Adjoint.ColorVertByWelshPowell() 
  WP_efficiency = (max(Graph_to_be_tested.vert_degree)/(Adjoint.numb_color))
  
  #-- end time measurement

  # wall-clock time
  wclock_etime = time.time()
      
  # -- saving the execution time
  Execution_time_WP = (wclock_etime - wclock_btime)
      
      
      
  # -- Apply DSATUR algorithm 

  # -- begin time measurement
  # wall-clock time
  wclock_btime = time.time()      
      
  print '\n---Coloring the graph_adjoint with DSATUR algorithm'
  Adjoint.ColorVertByDSATUR()  
  DSat_efficiency = (max(Graph_to_be_tested.vert_degree))/(Adjoint.numb_color)

  #-- end time measurement
  # wall-clock time
  wclock_etime = time.time()
      
  # -- saving the execution time
  Execution_time_DSat = (wclock_etime - wclock_btime)
   

  # ----------------------------------------------------------------------------
  # -- OUTPUT
  # ----------------------------------------------------------------------------

  # -- reporting errors
    
  print "\n", PROG_NAME,"Errors :"  
  print Graph_to_be_tested.err_msg
  print Adjoint.err_msg
  
  
  # -- saving the results in a text_file
    
  print '\n', PROG_NAME, '---WRITING RESULTS for the test on ', Graph_file_name
    
 
 # -- open file   
  result_file_name = "../data/out/benchmark_results.txt" 
  try :
      p_file = open(result_file_name, "a")
  except :
      print " Error: cannot open " + result_file_name
      return


  # -- write in
 
  # write the name of the graphs tested
  p_file.write("\n\n\nNEW TEST for the Graph " + Graph_file_name)
  p_file.write("\n\tnodes : " + str(Graph_to_be_tested.numb_vert))
  p_file.write("\n\tlinks : " + str(Graph_to_be_tested.numb_edge))
  p_file.write("\n\tdegree max : " + str(max(Graph_to_be_tested.vert_degree)))
  
  # structure statistics
  L1 = numpy.array(Graph_to_be_tested.vert_degree)
  S1 = float(sum(L1))
  L2 = L1*L1
  S2 = float(sum(L2))
  l = len(L1)
  
  p_file.write("\n\n ---STRUCTURE---")
  average_degree = "%.2f" % (S1/l)
  standard_deviation_among_degrees = "%.2f" % (math.sqrt((S2/l) - (S1*S1)/(l*l)))
  p_file.write("\n\taverage degree : " + str(average_degree))
  p_file.write("\n\tstandard deviation among degrees : " + str(standard_deviation_among_degrees))
  
  
  p_file.write("\n\n ---RESULTS---")
  # write the Welsh-Powell efficiency
  print 'WP_efficiency ', WP_efficiency
  p_file.write('\n\n\tWP-efficiency' + "\t" + str(WP_efficiency))
        
  # write the DSATUR efficiency
  print 'DSat efficiency', DSat_efficiency
  p_file.write('\n\tDSat-efficiency' + "\t" + str(DSat_efficiency))
      
  # write the Welsh-Powell execution time     
  print'Execution_time_WP', Execution_time_WP
  p_file.write('\n\tWP-execution time' + "\t" + str(Execution_time_WP))
     
  # write the DSATUR execution time
  print 'Execution_time_DSat', Execution_time_DSat
  p_file.write('\n\tDSat-execution time' + "\t" + str(Execution_time_DSat))

  
  return EXIT_SUCCESS

# END def main ( argc, argv ) :
# ------------------------------------------------------------------------------

Adresses_Of_The_Graphs_To_Test = ["../data/in/MRG/AP3D-H0750B-S0-LGMb120.vtk"]
                                  
for k in range (len(Adresses_Of_The_Graphs_To_Test)):
    main(PROG_NAME, Adresses_Of_The_Graphs_To_Test[k])