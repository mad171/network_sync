# -*- coding: utf-8 -*-

##
# @author L. Descateaux, IS1260-PC01, CentraleSupelec, France
# @date 2015-12-11, 2016-02-08
# @date 2016-12-13, 2016-12-13
# @date Première modification : 2016-12-13, Derniere modification 2017-02-10
# @version 2.0
#
# @class Graph
#

# -- Third-party modules
import numpy
import scipy.sparse

##
# @brief A set of graph algorithms and basic operations.
#
# Currently:
# - File Input (DIMACS)
# - Line graph
# - Coloring (Welsh-Powell)
#
class Graph ( object ) :

  # ----------------------------------------------------------------------------
  # -- CLASS ATTRIBUTES
  # ----------------------------------------------------------------------------

  # Error status
  SUCCESS = 0
  FAILURE = 1

  # Class description
  CLASS_NAME = "Graph"
  CLASS_AUTHOR = "L. Descateaux, IS1260-PC01, CentraleSupelec, France"
  METHODS = """
  __init__ (
        self,
        numb_vert = 0,
        numb_edge = 0,
        edge2vert = numpy.array([]),
        p_edge2vert = numpy.array([0]) )

  ReadFromFileDimacs (
        self,
        file_name,
        file_type = "ASCII" )
  ReadFromFileDimacsAscii (
        self,
        file_name )

  BuildEdge2Edge ( self )

  BuildVertDegree ( self )
  
  ColorVertByWelshPowell ( self )
  CorlorVertByDSATUR ( self )

  BuildVert2Edge ( self )
  SortVert2EdgeByColor ( self )
  BuildVert2Vert ( self )
  """

  # ----------------------------------------------------------------------------
  # -- INITIALIZATION
  # ----------------------------------------------------------------------------

  ##
  # @param numb_vert = number of vertices
  #             [default: 0]
  # @param numb_edge = number of edges
  #             [default: 0]
  # @param edge2vert = vertices of each edge (CSR storage)
  #             [default: numpy.array([])]
  # @param p_edge2vert = index of each edge in edge2vert (CSR storage)
  #             [default: numpy.array([0])]
  # @remarks There is not any copy of the input arrays.
  #
  def __init__ (
        self,
        numb_vert = 0,
        numb_edge = 0,
        edge2vert = numpy.array([]),
        p_edge2vert = numpy.array([0]) ) :

    # -- Dataset

    self.numb_vert = numb_vert
    self.numb_edge = numb_edge
    self.edge2vert = edge2vert
    self.p_edge2vert = p_edge2vert

    # -- Line graph

    # Adjacent edges of each edge (CSR storage)
    self.edge2edge = numpy.array([])
    self.p_edge2edge = numpy.array([0])

    # -- Graph coloring

    # Degree of each vertex
    self.vert_degree = numpy.array([])

    # Adjacent vertices of each vertex (CSR storage)
    self.vert2vert = numpy.array([])
    self.p_vert2vert = numpy.array([0])

    # Number of colors
    self.numb_color = 0

    # Color of each vertex
    self.vert_color = numpy.array([])

    # Color of each edge
    self.edge_color = numpy.array([])

    # -- Color-based reordering

    # Incident edges of each vertex (CSR storage)
    self.vert2edge = numpy.array([])
    self.p_vert2edge = numpy.array([0])

    # -- Error handling

    # Last error code
    self.err_code = Graph.SUCCESS

    # Last error message
    self.err_msg = ""

  # END def __init__ (
#        self,
#        numb_vert = 0,
#        numb_edge = 0,
#        edge2vert = numpy.array([]),
#        p_edge2vert = numpy.array([0]) ) :
  # ----------------------------------------------------------------------------

  # ----------------------------------------------------------------------------
  # -- FILE I/O
  # ----------------------------------------------------------------------------

  ##
  # @brief Reads graph dataset from a DIMACS edge problem file.
  # @param file_name = full name of the file (str)
  # @param file_type = numerical data format ("ASCII", "BINARY")
  #             [default: "ASCII"]
  #
  def ReadFromFileDimacs (
        self,
        file_name,
        file_type = "ASCII" ) :

    # -- init

    # error handling
    self.err_code = Graph.SUCCESS
    self.err_msg += ""
    err_header = "*** [" + Graph.CLASS_NAME + ".ReadFromFileDimacs]"

    # -- read dataset
    if (file_type == "ASCII") :

      self.ReadFromFileDimacsAscii(file_name)
      if (self.err_code == Graph.FAILURE) :
        self.err_msg = err_header + "\n" + self.err_msg

    else :
      self.err_code = Graph.FAILURE
      self.err_msg = err_header + " Error: " + file_type + " file not supported"


    return

  # END def ReadFromFileDimacs (
#        self,
#        file_name,
#        file_type = "ASCII" ) :
  # ----------------------------------------------------------------------------

  ##
  # @brief Reads graph dataset from a DIMACS edge problem ASCII file.
  # @param file_name = full name of the file (str)
  #
  def ReadFromFileDimacsAscii (
        self,
        file_name ) :


    # -- init

    # error handling
    self.err_code = Graph.SUCCESS
    self.err_msg += "\n"
    err_header = "*** [" + Graph.CLASS_NAME + ".ReadFromFileDimacsAscii]"

    # output
    self.numb_vert = 0
    self.numb_edge = 0
    self.edge2vert = numpy.array([])
    self.p_edge2vert = numpy.array([0])


    # -- open file
    try :
      p_file = open(file_name, "r")
    except :
      self.err_code = Graph.FAILURE
      self.err_msg += err_header + " Error: cannot open " + file_name
      return


    # -- read number of vertices and number of edges

    # read problem line (p edge numb_vert numb_edge)
    buf_line = p_file.readline()
    while (buf_line[0] not in {'', 'p'}) :
      buf_line = p_file.readline()
    
    # extract numbers
    buf = buf_line.split()
    self.numb_vert = int(buf[2])
    self.numb_edge = int(buf[3])
    
    
    # -- read edges vertices

    # init
    self.edge2vert = []
    self.p_edge2vert = [0]
    edge_count = 0

    # read edge descriptor lines (e vert1 vert2)
    buf_line = p_file.readline()
    buf = buf_line.split()
    while ( buf != [] ) :
        vert1 = int(buf[1])
        vert2 = int(buf[2])
        edge_count += 1
        self.edge2vert.append(vert1-1)
        self.edge2vert.append(vert2-1)
        self.p_edge2vert.append(self.p_edge2vert[-1] + 2)
        buf_line = p_file.readline()
        buf = buf_line.split()


    # -- convert to numpy.ndarray
    self.edge2vert = numpy.array(self.edge2vert)
    self.p_edge2vert = numpy.array(self.p_edge2vert)
    
    
    # -- update number of edges
    self.numb_edge = edge_count
    print " --> Edge2Vert built\n\n"

    # -- close file
    p_file.close()

    return

  # END def ReadFromFileDimacsAscii (
#        self,
#        file_name ) :
  # ----------------------------------------------------------------------------

  # ----------------------------------------------------------------------------
  # -- LINE GRAPH
  # ----------------------------------------------------------------------------

  ##
  # @brief Builds edge2edge and p_edge2edge from edge2vert
  #
  def BuildEdge2Edge ( self ) :

    # -- init

    # error handling
    self.err_code = Graph.SUCCESS
    self.err_msg += "\n"
    err_header = "*** [" + Graph.CLASS_NAME
    err_header += ".BuildEdge2Edge]"
    
    # output
    self.edge2edge = numpy.array([])
    self.p_edge2edge = numpy.array([0])


    # -- check dependencies
    if (len(self.edge2vert) == 0) :
      self.err_code = Graph.FAILURE
      self.err_msg += err_header + " Error: edge2vert is required"
      return

    # -- build transpose of incidence matrix 
    data = numpy.ones(len(self.edge2vert))
    incidence_matrix_transpose = scipy.sparse.csr_matrix((data, self.edge2vert,
                                                          self.p_edge2vert)).toarray()
    

    # -- compute incidence matrix
    incidence_matrix = incidence_matrix_transpose.transpose()


    # -- build identity matrix
    identity = scipy.sparse.identity(self.numb_edge)


    # -- compute line graph adjacency matrix 
    produit1 = incidence_matrix_transpose.dot(incidence_matrix)
    produit = scipy.sparse.csr_matrix(produit1)
    adjacency_matrix = produit - 2*identity


    # -- extract edge2edge and p_edge2edge
    self.edge2edge = adjacency_matrix.indices
    self.p_edge2edge = adjacency_matrix.indptr
    
    print '--> Edge2Edge built\n\n'
                
    return

  # END def BuildEdge2Edge ( self ) :
  # ----------------------------------------------------------------------------

  # ----------------------------------------------------------------------------
  # -- COLORING
  # ----------------------------------------------------------------------------

  ##
  # @brief Builds vert_degree.
  #
  def BuildVertDegree ( self ) :

    # -- init

    # error handling
    self.err_code = Graph.SUCCESS
    self.err_msg += "\n"
    err_header = "*** [" + Graph.CLASS_NAME
    err_header += ".BuildVertDegree]"
      
    # output
    self.vert_degree = numpy.array([])
    
    
    # -- check dependencies
    if ((len(self.edge2vert) == 0)
        and (len(self.vert2edge) == 0)
        and (len(self.vert2vert) == 0)) :
      self.err_code = Graph.FAILURE
      self.err_msg += err_header + " Error: either edge2vert or vert2edge"
      self.err_msg += " or vert2vert is required"
      return


    # -- build vert_degree
    
    # either build from vert2vert
    if (len(self.vert2vert) != 0) :
        self.numb_vert = len(self.p_vert2vert)-1
        self.vert_degree = numpy.array([0]*self.numb_vert)
        for k in range (1,len(self.p_vert2vert)):
            self.vert_degree[k-1] = self.p_vert2vert[k]-self.p_vert2vert[k-1]
            
    # or build from vert2edge
    elif (len(self.vert2edge) != 0) :
        self.numb_vert = len(self.p_vert2edge)-1
        self.vert_degree = numpy.array([0]*self.numb_vert)
        for k in range (1,len(self.p_vert2edge)):
            self.vert_degree[k-1] = self.p_vert2edge[k]-self.p_vert2edge[k-1]        
        
    # or build from edge2vert
    else :
        self.numb_vert = max(self.edge2vert)+1
        self.vert_degree = numpy.zeros((self.numb_vert))
        for k in range (len(self.edge2vert)):
            self.vert_degree[self.edge2vert[k]] += 1
    
    print '--> Vert_Degree built\n\n'
    
    return

  # END def BuildVertDegree ( self ) :
  # ----------------------------------------------------------------------------

  ##
  # @brief Constructs a proper graph coloring using Welsh-Powell algorithm.
  #
  def ColorVertByWelshPowell ( self ) :

    # -- init

    # error handling
    self.err_code = Graph.SUCCESS
    self.err_msg += "\n"
    err_header = "*** [" + Graph.CLASS_NAME
    err_header += ".ColorVertByWelshPowell]"
      
    # output
    self.vert_color = numpy.array([])
    self.numb_color = 0
    
 
    # -- check dependencies
    if (len(self.vert2vert) == 0) :
        self.err_code = Graph.FAILURE
        self.err_msg += err_header + " Error: vert2vert is required"
        return
    if (self.numb_vert <= 0) :
        self.err_code = Graph.FAILURE
        self.err_msg += err_header + " Error: numb_vert is required" 
        return
    if (len(self.vert_degree) != self.numb_vert) :
        self.err_code = Graph.FAILURE
        self.err_msg += err_header + " Error: vert_degree is required"
        return
    
    
    # -- initializing
    self.vert_color = numpy.array([-1]*self.numb_vert)
    vert_degree_copy= numpy.array([0]*(len(self.vert_degree)))
    for i in range (len(vert_degree_copy)):  
        vert_degree_copy[i] = self.vert_degree[i]
    Sort = []
    numb_colored_verts = 0
    
 
    # -- sort vertices by decreasing degree 
    for i in range (len(vert_degree_copy)):
        max_degree = 0
        max_degree_indice = 0

        # adds the vert with degree max in Sort
        for k in range (len(vert_degree_copy)):
            if (vert_degree_copy[k] > max_degree):
               max_degree = vert_degree_copy[k]
               max_degree_indice = k
               
        Sort.append(max_degree_indice)
        vert_degree_copy[max_degree_indice] = 0      
    
    # -- apply Welsh-Powell algorithm to Sort
    while ((-1 in self.vert_color) and (numb_colored_verts < self.numb_vert)):
        
        for k in range (len(Sort)):
          
            colorable = 1
            V = Sort[k]
            
            # V has to be uncolored
            if self.vert_color[V] != -1 :
                colorable = 0
                
            # Moreover it has to be next to other vertices which aren't colored
            # in the current color (numb_color)
            if colorable == 1 :
                for i in range (self.p_vert2vert[V],self.p_vert2vert[V+1]):
                    if self.vert_color[self.vert2vert[i]] == self.numb_color :
                        colorable = 0
            
            # If the 2 previous conditions are verified, 
            # color V with the current color
            if colorable == 1 :
                self.vert_color[V] = self.numb_color
                numb_colored_verts +=1
        
        
        self.numb_color += 1           
        
        
    print '--> FIN COLORATION WP\n\n'
    
    return

  # END def ColorVertByWelshPowell ( self ) :
  # ----------------------------------------------------------------------------


  ##
  # @brief Constructs a proper vertex coloring using DSATUR algorithm.
  #
  def ColorVertByDSATUR ( self ) :

    # -- init

    # error handling
    self.err_code = Graph.SUCCESS
    self.err_msg += "\n"
    err_header = "*** [" + Graph.CLASS_NAME
    err_header += ".ColorVertByDSATUR]"
      
    # output
    self.vert_color = numpy.array([])
    self.numb_color = 0
    
    
    # -- check dependencies
    if (len(self.vert2vert) == 0) :
        self.err_code = Graph.FAILURE
        self.err_msg += err_header + " Error: vert2vert is required"
        return
    if (self.numb_vert <= 0) :
        self.err_code = Graph.FAILURE
        self.err_msg += err_header + " Error: numb_vert is required"
        return
    if (len(self.vert_degree) != self.numb_vert) :
        self.err_code = Graph.FAILURE
        self.err_msg += err_header + " Error: vert_degree is required"
        return
        
    
    # -- initializing
    self.vert_color = numpy.array([-1]*self.numb_vert)
    vert_dsat = numpy.array([0]*self.numb_vert)
    numb_colored_verts = 0
    
    
    # -- apply DSATUR algorithm to Sort
    while(-1 in self.vert_color):
    
        # -- choose the vert with DSAT MAX (and degree max if there are several
        # -- DSAT MAX vertices) 
    
            # seeks the n° of the first uncolored vert 
        indice = 0
        while (self.vert_color[indice] != -1):
            indice +=1
        
            # seeks the n° of the first dsat max vert and with the degree max
            # among the remaining verts
        DsatMax = vert_dsat[indice]
        i = indice +1
        for k in range (i, self.numb_vert):
            if self.vert_color[k] == -1 :
                if vert_dsat[k] > DsatMax :
                    indice = k
                    DsatMax = vert_dsat[k]
                elif vert_dsat[k] == DsatMax :
                    if self.vert_degree[k] > self.vert_degree[indice] :
                        indice = k
                        DsatMax = vert_dsat[k]
        
        
        # -- colors this vert with the smallest possible color
        adj_colors = numpy.array([-1]*(self.numb_color+1))
        
            #switch to 1 the colors used near the vert we want to color
            #and update of vert_dsat
        for k in range (self.p_vert2vert[indice],self.p_vert2vert[indice+1],1):
            Vadj = self.vert2vert[k]
            if self.vert_color[Vadj] != -1 :   
                adj_colors[self.vert_color[Vadj]] = 1
            vert_dsat[Vadj] += 1

        compteur = 0
        while(self.vert_color[indice] == -1):
            if adj_colors[compteur] == -1 :
                self.vert_color[indice] = compteur
                if compteur == (self.numb_color) :                
                    self.numb_color +=1 
            compteur +=1
        
        
        numb_colored_verts +=1    
    
    print "--> FIN COLORATION DSATUR\n\n"
    return

  # END def ColorVertByDSATUR ( self ) :
  # ---------------------------------------------------------------------------

  # ---------------------------------------------------------------------------
  # -- ORDERING
  # ---------------------------------------------------------------------------

  ##
  # @brief Builds vert2edge and p_vert2edge.
  #
  def BuildVert2Edge ( self ) :

    # -- init

    # error handling
    self.err_code = Graph.SUCCESS
    self.err_msg += "\n"
    err_header = "*** [" + Graph.CLASS_NAME
    err_header += ".BuildVert2Edge]"
    
    # output
    self.vert2edge = numpy.array([])
    self.numb_vert = max(self.edge2vert)+1
    self.p_vert2edge = numpy.array([0]*(self.numb_vert+1))


    # -- check dependencies
    if (len(self.edge2vert) == 0) :
      self.err_code = Graph.FAILURE
      self.err_msg += err_header + " Error: edge2vert is required"
      return
      
      
    # -- build vert2edge from edge2vert
    L= []
    for k in range (self.numb_vert) :
        L.append([])
    
    for k in range (0, len(self.edge2vert), 2) :
        L[self.edge2vert[k]].append(k//2)
        L[self.edge2vert[k+1]].append(k//2)
        
    for k in range (self.numb_vert):
        self.p_vert2edge[k+1] = len(L[k]) + self.p_vert2edge[k]       
      
    self.vert2edge = [val for sublist in L for val in sublist]
    self.vert2edge = numpy.array(self.vert2edge)
    
    print "--> Vert2Edge built\n\n"   
    return

  # END def BuildVert2Edge ( self ) :
  # ----------------------------------------------------------------------------

  ##
  # @brief Sorts list of incident edges of each vertex (vert2edge), according
  #        to edge color.
  #

  def SortVert2EdgeByColor ( self ) :

    # -- init

    # error handling
    self.err_code = Graph.SUCCESS
    self.err_msg += "\n"
    err_header = "*** [" + Graph.CLASS_NAME
    err_header += ".SortVert2EdgeByColor]"


    # -- check dependencies
    if (len(self.vert2edge) == 0 or len(self.edge_color) == 0) :
      self.err_code = Graph.FAILURE
      self.err_msg += err_header + " Error: vert2edge and edge_color are required"
      return


    # -- define functions to apply a fusion sort
    def fusion(T1,T2):
        if len(T1)==0:
            return T2
        elif len(T2)==0:
            return T1
        else:
            if self.edge_color[T1[0]] <= self.edge_color[T2[0]] :
                return numpy.concatenate(([T1[0]],fusion(T1[1:],T2)), axis = 0)
            else:
                return numpy.concatenate(([T2[0]],fusion(T1,T2[1:])), axis = 0)

    def tri_fusion(T):
        n=len(T)
        if n<=1:
            return T
        else:
            T1,T2 = T[:n//2],T[n//2:]
            return(fusion(tri_fusion(T1),tri_fusion(T2)))

    
    # -- sort vert2edge using the fusion sort
    for i in range(len(self.p_vert2edge)-1):
        AdjEdges = self.vert2edge[self.p_vert2edge[i]:self.p_vert2edge[i+1]]
        AdjTri = tri_fusion(AdjEdges)
        self.vert2edge[self.p_vert2edge[i]:self.p_vert2edge[i+1]] = AdjTri
        
    print "--> Vert2Edge sorted\n\n"
    return

  # END def SortVert2EdgeByColor ( self ) :
  # ----------------------------------------------------------------------------

  ##
  # @brief Builds vert2vert and p_vert2vert so that the sort apllied before remains correct
  #
  def BuildVert2Vert ( self ) :

    # -- init

    # error handling
    self.err_code = Graph.SUCCESS
    err_header = "*** [" + Graph.CLASS_NAME
    err_header += ".BuildVert2Vert]"
    
    # output
    self.vert2vert = numpy.array([],dtype = int)
    self.p_vert2vert = numpy.array([0])


    # -- check dependencies
    if (len(self.vert2edge) == 0 or len(self.p_vert2edge) == 0) :
      self.err_code = Graph.FAILURE
      self.err_msg += "\n"
      self.err_msg += err_header + " Error: vert2edge AND p_vert2edge are required"

      return
      
      
    # -- build vert2vert from vert2edge (and keeps its sort)  
    for Vert in range (len(self.p_vert2edge)-1):
        for i in range(self.p_vert2edge[Vert], self.p_vert2edge[Vert+1]):
            AdjEdge = self.vert2edge[i]
            for j in range (len(self.vert2edge)):
                if j!=i:
                    Edge = self.vert2edge[j]
                    if Edge == AdjEdge :
                        AdjVert = 0
                        while (self.p_vert2edge[AdjVert]<=j):
                            AdjVert+=1
                        self.vert2vert = numpy.concatenate((self.vert2vert,[int(AdjVert-1)]),axis=0)

    # -- p_vert2vert is equal to p_vert2edge
    self.p_vert2vert = self.p_vert2edge
    
    print "--> Vert2Vert built\n\n"
    return

  # END def BuildVert2Vert ( self ) :
  # ----------------------------------------------------------------------------

# END class Graph ( object ) :
# ------------------------------------------------------------------------------