# -*- coding: utf-8 -*-

##
# @author Ludovic Descateaux, IS1260-PC01, CentraleSupelec, France
# @date 2017-01-17, 2017-02-08
# @version 2.0 [Python 2.7]
#

# -- Standard modules
import numpy
import sys


# -- MRG modules
sys.path.append("mod")
from mesh import Mesh
from meshviz import MeshViz


# Program description
PROG_NAME = "[test MeshViz]"
Mesh_file_name = "../data/in/MRG/AP3D-H0750B-S0-LGM120.vtk"

# -- main ----------------------------------------------------------------------
def main (PROG_NAME, Mesh_file_name) :

  # ----------------------------------------------------------------------------
  # -- INITIALIZATION
  # ----------------------------------------------------------------------------

  # -- Graph handler
  
  Mesh_to_be_tested = Mesh()
  MeshViz1 = MeshViz()
  
  
  # ----------------------------------------------------------------------------
  # -- INPUT
  # ----------------------------------------------------------------------------

  # -- read mesh and set vizualisation parameters

  Mesh_to_be_tested.ReadFromFileVtk(Mesh_file_name)

  MeshViz1.ReadMeshFromFile(Mesh_file_name)
    
  MeshViz1.SelectPointData("subdomain_numb_nodes")  
  MeshViz1.SelectCellData("subdomain2numb_interface_node")

  MeshViz1.Config (bg_color = (0,0,0),
                   win_size = (800, 600),
                   win_pos = (20, 50),
                   cam_azimuth = 0,
                   cam_elevation = 0,
                   cam_zoom =  1.5)
  MeshViz1.BuildGlyph (sphere_radius = 1.5) 
  
  # -- Set Loads of work for nodes
  
  nodes_loads_2D = Mesh_to_be_tested.ReadFieldFromFileVtk(Mesh_file_name, "subdomain_numb_nodes")
  nodes_loads = numpy.array([0]*len(nodes_loads_2D))
  for k in range (len(nodes_loads_2D)) :
      nodes_loads[k] = nodes_loads_2D[k][0]
  MeshViz1.BuildPointColorScale (0,max(nodes_loads),scale_type = "rainbow" )
  
  
  # -- Set Loads of work for edges
  
  edges_loads_2D = Mesh_to_be_tested.ReadFieldFromFileVtk(Mesh_file_name, "subdomain2numb_interface_node")
  edges_loads = numpy.array([0]*len(edges_loads_2D))
  for k in range (len(edges_loads_2D)):
      edges_loads[k] = edges_loads_2D[k][0]
  MeshViz1.BuildCellColorScale (0,max(edges_loads),scale_type = "rainbow" )
  
  
  # -- Draw a visualization
  MeshViz1.IRender(nodes_loads,edges_loads) 
  MeshViz1.Close()

  return

# END def main ( argc, argv ) :
# ------------------------------------------------------------------------------

main(PROG_NAME, Mesh_file_name)
