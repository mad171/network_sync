# Dense Network Synchronization - Scheduling Algorithms Benchmark

Network Sync provides a benchmark to figure the most efficient way to perform a blocking communication between all the nodes of your network.

# Network Sync
Network Sync is a software aiming at optimizing the total time of a communication process within a network of servers. It also allows you to simulate and visualize a communication cycle in your network.

Each server has a list of neighboring servers to which it has to transmit a certain amount of information so that the network is considered to be synchronized. Here are the conditions necessary so that Network Sync is relevant for the scheduling of the communications of your network:

- A server can only communicate one by one at a time.

- The transmissions are formally bidirectional. That is to say that once a transfer between 2 servers is carried out, the information has been transmitted in both directions and the amount of information sent is the same in both directions.


- There is no required order among communications between servers. For example, for a given server no.Z, we can choose to initiate connection with server no.X before or after connection no.Y depending on the optimality for a short network cycle time.

- Communication are blocking. That is to say that when a server #X's next target is another server #Y, server #X will wait without doing anything until #Y has also #X as its next contact.

![Network Synchronization Visualization](./assets/graph_sync.jpg)
- *Each sphere represents a node. A node painted in 'dark_blue' is available for communication, otherwise it is in local processing. The local workload progress of a node is represented by its color scale:*
  - *['light_blue'=beginning_local_workload] --> ['green'=progressing_local_workload] --> ['red'=finishing_local_workload].*
- *In the same way, each link between two nodes represents a communication that has to be performed to ensure the complete synchronization of the network. A link in 'dark_blue' means that the communication is not set currently between the two nodes. Otherwise, the communication workload's progress once the link is activated is represented by its color scale:*
  - *['light_blue'=begin_transmition] --> ['green'=progressing_communication] --> ['red'='finishing_tranmission]*
- A node is only available for a communication once it has performed its local workload. The communications can only be set one-by-one.


# Table of Content: 

- [Dense Network Synchronization - Scheduling Algorithms Benchmark](#dense-network-synchronization---scheduling-algorithms-benchmark)
- [Network Sync](#network-sync)
- [Table of Content:](#table-of-content)
- [Exemple of complete Vizualization](#exemple-of-complete-vizualization)
- [Projects Content](#projects-content)
- [User guide](#user-guide)
  - [a. Required setup](#a-required-setup)
  - [b. What should be the format of your network architecture description](#b-what-should-be-the-format-of-your-network-architecture-description)
  - [c. Which Algorithm is the best for your network ?](#c-which-algorithm-is-the-best-for-your-network)
  - [d. Run a Simulation of your network's synchronization](#d-run-a-simulation-of-your-networks-synchronization)
  - [e. Retrieve your results](#e-retrieve-your-results)


# Exemple of complete Vizualization
![Click here to download a video showing an exemple of synchronization (it's a bit heavy)](https://gitlab.com/mad171/network_sync/blob/master/assets/Exemple_de_rendu_video_LGM120_DSATUR.avi)


# Projects Content  

- /Data
    - In 
        - DSJ, /LEI, /CUL : folders containing .col files, formatted text files listing network connections.
        - MRG : folder containing .vtk files, formatted text files listing connections, spatial positions, and workloads within networks.
    - Out 
        - Visualisation : folder containing the screenshots of the different simulations executed with different scheduling algorithms.
        - benchmark_results.txt : output document of the results of the nco_benchmark program.
        - Visu_resultats_benchmark.xls : comparative analysis of two coloration algorithms based on network connections (DSATUR and WELSH-POWELL).
        - simulation_results.txt : output document of the results of the nco_benchmark program.
        - simulation_ordre_des_connexions.txt : output document of the connection orders recommended by the algorithm during the last simulation.
        - Visu_resultats_simu.xls : comparative analysis of the total synchronization time with the different algorithms.
- /Dev
	- /Mod
        - Graph : "Graph" class, creating digital objects representing the graphs to be processed, as well as elementary operations.
        - Mesh : "Mesh" class, creating digital objects representing the meshes to be treated, as well as elementary operations.
        - Netsim : "Netsim" class, creating digital objects on which we can perform simulation steps.
        - MeshViz : "MeshViz" class, creating objects similar to meshes, and grouping methods allowing to obtain a configurable visualization of the network via the Paraview software.
    - nco_benchmark : benchmark program giving optimal coloring according to Welsh-Powell and DSATUR in their application on a graph.
    - nco_simulation2 : simulation and visualization program for synchronizing a network of servers, according to a given color.
    - Pre_meshViz : network preview program, used to choose a correct visual setting before launching the simulation.
    - GraphTest… : test programs for the proper functioning of the different methods of the Graph class.
    - MeshTest… : test programs for the proper functioning of the various methods of the Mesh class.



# User guide

## a. Required setup

1 – Download and install Python version 2.7, and a suited IDE (Spyder, Pyzo, …). https://sourceforge.net/projects/winpython/files/WinPython_2.7/2.7.10.3/ 
2 – Download and install the visualization software Paraview. Save it in C:/Programme Files. http://www.paraview.org/download/ 
3 – Make sure you reference it in your environment variable PYTHONPATH : 
-	add the path C:\Program Files (x86)\ParaView 5.3.0-RC2-Qt5-OpenGL2-Windows-64bit\bin\Lib\site-packages
-	add the path C:\Program Files (x86)\ParaView 5.3.0-RC2-Qt5-OpenGL2-Windows-64bit\bin\Lib\site-packages\vtk 
4 – If you wish to compile the simulation's images into a movie, you can download AfterEffect and help yourself with "Stop Motion" tutorials to learn the relevant manipulation.


## b. What should be the format of your network architecture description

Network Sync works on networks whose structure is written in files in .vtk format, suitable for storing meshes. In order to run Netsync on your network, import it in .vtk format into the ..data / in / MRG folder. You will find examples of networks already present in the MRG folder, and you can also find out about the structure of vtk-type files at http://www.vtk.org/wp-content/uploads/2015/04/file- formats.pdf.
Preview of a .vtk file

![Preview of a .vtk file](.\assets\vtk_preview.png)


## c. Which Algorithm is the best for your network ?

Once your network has been imported into vtk format in the MRG folder, you can find the most optimal scheduling algorithm. Netsync offers two different algorithms: Welsh-Powell, and DSATUR. In order to determine the best advantage for your network, you can either use the comparative study made on the Excel file Visu_resultats_benchmark.xls, or run a benchmark script on your network specifically. 

- Find in the list, a network whose structure is close to yours, and see which algorithm is best in this case. [Link to excel file](./data/out/Visu_resultats_simu.xslx)


- Run the benchmark script on your graph. To do this, open the nco_benchmark program with your Python code editor, then replace the address of the file to be tested before running the program. ![Run the benchmark script](.\assets\run_benchmark.png) The result of the benchmark applied to your graph will then be written to the text file benchmark_results.txt in the following form. This will allow you to know which algorithm is the most efficient for your network. ![Content from the execution of the Benchmark](./assets/benchmark_summary.png)

## d. Run a Simulation of your network's synchronization

Now that you have chosen the algorithm, you can perform a simulation of the synchronization of your network. Beforehand, you must first preview and configure the visual rendering of your network. To do this, use the Pre_meshViz.py script.

First change the address of the file in "# Program description" (line 22).[Network description vtk file](./assets/visual_setting.png)

Then try values of display parameters until you get a satisfactory view of your network by running the script. ![Visual settings](./assets/visual_setting2.png)
![Visual settings 2](./assets/visual_setting3.png)

You now know which visualization parameters you can choose for the simulation. It's time to run your network synchronization simulation. Open the nco_simulation program and transfer the settings you previously determined in the code part "# - setting the visualization" (lines 164 to 169).
![Setup your simulation](./assets/setup_simu.png)

Also remember to change the address of the vtk file, as well as the coloring algorithm to apply in "Program description".


You can even configure the simulation so that the viewing angle changes over time. To do this, set an increment value for the azimuth or elevation angles in the settings inside the simulation while loop.

Finally, in this same simulation loop, set the number of simulation steps between each screenshot. I advise you to look in your vtk file for the top or the least loaded stop. Then set the number of simulation steps between screenshots to 1 / 5th of this value under the “if (counter%…)” condition. For example, if the least loaded element of your network is a stop whose load is 250, take screenshots every 50 simulation steps. ![Setup you simulation 2](./assets/setup_simu2.png)

## e. Retrieve your results

By running the program as well, you will get several results:
- Real-time visualization of network synchronization
- Successive screenshots in the ..data / out / Visualization / Bulk folder. (Make sure to empty it first, then move the result to another folder after the simulation)
- The results of the simulation (simulation time) written in the output text file simulation_results.txt
- The scheduling of connections according to the algorithm applied in the file simulation_order_des_connections.txt
![Network Visualization](./assets/network_sync.png)

You then have everything in hand to optimize the synchronization time of your network!








